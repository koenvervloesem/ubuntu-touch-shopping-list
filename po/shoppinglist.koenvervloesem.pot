# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the shoppinglist.koenvervloesem package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: shoppinglist.koenvervloesem\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-04-27 18:41+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/AboutDialog.qml:7 ../qml/Main.qml:187
msgid "About"
msgstr ""

#: ../qml/AboutDialog.qml:12
msgid ""
"This is an example shopping list app designed to teach you Ubuntu Touch app "
"development."
msgstr ""

#: ../qml/AboutDialog.qml:16
msgid "Close"
msgstr ""

#: ../qml/Main.qml:137
msgid "Remove all items"
msgstr ""

#: ../qml/Main.qml:138 ../qml/Main.qml:148
msgid "Are you sure?"
msgstr ""

#: ../qml/Main.qml:147
msgid "Remove selected items"
msgstr ""

#: ../qml/Main.qml:169 shoppinglist.desktop.in.h:1
msgid "Shopping List"
msgstr ""

#: ../qml/Main.qml:170
msgid "Never forget what to buy"
msgstr ""

#: ../qml/Main.qml:183
msgid "Settings"
msgstr ""

#: ../qml/Main.qml:202
msgid "Add"
msgstr ""

#: ../qml/Main.qml:219
msgid "Shopping list item"
msgstr ""

#: ../qml/Main.qml:305
msgid "Remove all..."
msgstr ""

#: ../qml/Main.qml:311
msgid "Remove selected..."
msgstr ""

#: ../qml/OKCancelDialog.qml:11
msgid "OK"
msgstr ""

#: ../qml/OKCancelDialog.qml:20
msgid "Cancel"
msgstr ""
